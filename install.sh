#!/bin/bash


# ══════════════════════════════════════════════════════════════════════════════
printf '\n\n'
printf '\033[30m\033[43m         \033[0m\n'
printf '\033[30m\033[43m         \033[0m\n'
printf '\033[30m\033[43m         \033[0m\033[33m / consultores de tecnologia&design \033[0m\n'
printf '\033[30m\033[43m     x128\033[0m\033[33m / www.x128.com.br \033[0m\n'
# printf '\033[0;33m          // Possibilidades empolgantes, resultados admiráveis \033[0m\n'
printf '\n'
# ══════════════════════════════════════════════════════════════════════════════


header_step () {
    printf "\n\n\e[1;49;33m"
    printf '═%.0s' {1..80}
    printf "\n$1\n"
    printf '═%.0s' {1..80}
    printf "\e[0m\n"
}


# ══════════════════════════════════════════════════════════════════════════════
header_step 'System configuration'
# ══════════════════════════════════════════════════════════════════════════════
cd /root
yum -y update
sudo timedatectl set-timezone America/Sao_Paulo


# ══════════════════════════════════════════════════════════════════════════════
header_step 'SSH & Git configuration'
# ══════════════════════════════════════════════════════════════════════════════
yum install -y openssh-server
ssh-keygen -t ed25519 -C "server@pills.med.br" -N "" -f /root/.ssh/id_ed25519
# echo "" && echo ""
# cat /root/.ssh/id_ed25519.pub
printf "\n\n\e[5;101;93m ⚠  ACTION REQUIRED! \e[0m\n"
printf "Add the public key…\n"
printf "\e[0;40;31m%s\e[0m\n" "$(</root/.ssh/id_ed25519.pub)"
printf "…to the deploy keys at Pills repository settings at…\n"
printf "\e[0;40;34mhttps://gitlab.com/nicsware/pills/-/settings/ci_cd#js-deploy-keys-settings\e[0m\n"
read -p "…and press ENTER when done "
printf "\n"

# yum install -y http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.noarch.rpm
# yum install -y http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm
yum install -y http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm
yum install -y git
git config --global push.default simple
git config --global user.name "Pills production server"
git config --global user.email server@pills.med.br

ssh-keyscan gitlab.com >> /root/.ssh/known_hosts # not the most secure way, but the most practical one
# 'https://gitlab.com/nicsware/pills.git' asks for user&pass so we have to use SSH
ssh-agent bash -c 'ssh-add /root/.ssh/id_ed25519; git clone git@gitlab.com:nicsware/pills.git'


# ══════════════════════════════════════════════════════════════════════════════
header_step 'Exposys NodeJS Web Server'
# ══════════════════════════════════════════════════════════════════════════════
# curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
# nvm install v0.10.48
curl -sL https://rpm.nodesource.com/setup_0.10 | bash -
yum install -y nodejs
npm i -g pm2@2.0.3

# block under development
pm2 startup centos
pm2 install pm2-logrotate
pm2 reloadLogs
pm2 logrotate -u $USER

/home/safeuser/pills/app-pm2-start.sh


# ══════════════════════════════════════════════════════════════════════════════
header_step 'Neo4J Enterprise 2.0.2 graphical database'
# ══════════════════════════════════════════════════════════════════════════════
# yum install neo4j-enterprise-2.0.2 # life would be so easy if this works

yum install -y passwd
useradd safeuser
passwd safeuser -d
useradd neo4j
passwd neo4j -d

yum install -y java-1.8.0-openjdk
yum install -y which
yum install -y lsof

# //////////////////////////////////////////////////////////////////////////////
printf "\e[0;101;93m ⚠  ATENTION! work in progress here! \e[0m\n\n"
read -p "press ENTER to continue "
chown -R safeuser /home/safeuser/pills # ATTENTION HERE
/home/safeuser/pills/gdb/start.sh
exit 0
# //////////////////////////////////////////////////////////////////////////////


# ══════════════════════════════════════════════════════════════════════════════
header_step 'Cronjobs'
# ══════════════════════════════════════════════════════════════════════════════
yum install -y sudo yum install cronie
(crontab -u root -l 2>/dev/null; echo "@reboot /home/safeuser/pills/gdb/start.sh") | crontab -
(crontab -u root -l 2>/dev/null; echo "@daily /home/safeuser/pills/tools/sync-site.sh") | crontab -
# (crontab -u root -l 2>/dev/null; echo "0 0 * * * /home/safeuser/pills/tools/sync-site.sh") | crontab -


# ══════════════════════════════════════════════════════════════════════════════
header_step 'Optional customizations'
# ══════════════════════════════════════════════════════════════════════════════

# Aliases
alias ll='ls -lhAG'
echo "alias ll='ls -lhAG'" >> /root/.bash_profile

# FS iNotify
echo fs.inotify.max_user_watches=524288 >> /etc/sysctl.conf
sysctl -p

# nano editor
yum install -y nano
git config --global core.editor "nano"
echo "set nowrap" >>/etc/nanorc
cat <<EOF >>/root/.bash_profile
export VISUAL="nano"
export EDITOR="nano"
EOF

# Firewall
sudo firewall-cmd --state
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --permanent --add-service=https
firewall-cmd --reload
systemctl restart firewalld


# ══════════════════════════════════════════════════════════════════════════════
exit 1
