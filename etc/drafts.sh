#!/bin/bash
exit 0 # failsafe



# Docker on MacOS
docker-machine start default
docker-machine env
eval $(docker-machine env)
docker run -it --privileged centos bash

# Prelude script
alias ll='ls -lhAG'
yum install -y nano
nano ~/install-server-prelude.sh
chmod +x ~/install-server-prelude.sh
~/install-server-prelude.sh
# all together
alias ll='ls -lhAG' && yum install -y nano && nano ~/install-server-prelude.sh && chmod +x ~/install-server-prelude.sh && ~/install-server-prelude.sh



# Test webserver at port 80
cd ~
mkdir www
cd www
echo "Hello world" > index.html
yum install -y python36
#python -m SimpleHTTPServer 80
#python3 -m http.server 80
nohup python3 -m http.server 80 &
#curl "http://0.0.0.0:80"
curl "http://127.0.0.1"



	
# ══════════════════════════════════════════════════════════════════════════════

# ======================================
# ROOT
# ======================================

yum -y update
yum update -y nss curl libcurl

yum install -y git
git config --global push.default simple
git config --global user.name "Pills production server"
git config --global user.email server@pills.med.br

yum install -y nano

echo fs.inotify.max_user_watches=524288 >> /etc/sysctl.conf
sysctl -p

# curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
# nvm install v0.10.48

yum install java-1.8.0-openjdk

useradd safeuser
yum install -y passwd
passwd safeuser

ssh-keygen -t ed25519 -C "server@pills.med.br"
cat .ssh/id_ed25519
cat .ssh/id_ed25519.pub 

# really needed?
# rpm -Uvh https://s3.amazonaws.com/aaronsilber/public/authbind-2.1.1-0.1.x86_64.rpm
# touch /etc/authbind/byport/80
# chown safeuser /etc/authbind/byport/80
# chmod 755 /etc/authbind/byport/80
# authbind

# install pm2 missing?
pm2 startup systemd

#cp ./pills-neo4j.service /etc/systemd/system/
#systemctl daemon-reload
#systemctl start pills-neo4j

#EDITOR=nano crontab -u root -e
#EDITOR=nano crontab -u safeuser -e
chmod +x /home/safeuser/pills/app-start.sh 
(crontab -u root -l 2>/dev/null; echo "@reboot ~/pills/gdb/start.sh") | crontab -

# Ip Tables!?
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload

# Optional
yum install -y gcc-c++ make

git clone https://github.com/lexblagus/bash-git-prompt.git ./.bash-git-prompt
echo GIT_PROMPT_THEME="Coloured_Tags" >> ~/.bash_profile
echo source "$HOME/.bash-git-prompt/gitprompt.sh" >> ~/.bash_profile

# Please restart server




# ======================================
# SAFEUSER
# ======================================
git clone git@gitlab.com:nicsware/pills.git



# curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
# nvm install v0.10.48
# npm i -g pm2@2.0.2



# Optional
# git clone https://github.com/lexblagus/bash-git-prompt.git ./.bash-git-prompt
# echo GIT_PROMPT_THEME="Coloured_Tags" >> ~/.bash_profile
# echo source "$HOME/.bash-git-prompt/gitprompt.sh" >> ~/.bash_profile
